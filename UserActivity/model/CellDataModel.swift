import UIKit
struct CellDataModel {
    var dayDate: String
    var stepsCounter: Int
    var stepsTarget: Int
    var walkSteps: Int
    var aerobicSteps: Int
    var runSteps: Int
    var isReached: Bool
    var bar: [CGFloat]
}
