import UIKit

class MainViewController: UIViewController {
    var myTableView: UITableView = {
        let table = UITableView()
        return table
    }()
    var cellData = [CellDataModel]()
    let urlStorage = API_SECOND_URL
    var jsonData = [JSONData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Navbar section
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_aim")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(alertFunction))
        let rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_plus")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: nil)
        title = "Steps"
        navigationItem.leftBarButtonItem = leftBarButtonItem
        navigationItem.rightBarButtonItem = rightBarButtonItem
        // Get JSON data
        getJSONfromURL()
        // Init cell data section
        view.backgroundColor = UIColor.white
        view.addSubview(myTableView)
        self.myTableView.backgroundColor = .clear
    }
}

extension MainViewController {
    
    func tableViewSetup() {
        let rectFrame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)        
        myTableView.frame = rectFrame
        myTableView.tableFooterView = UIView()
        myTableView.rowHeight = UITableView.automaticDimension
        myTableView.contentInset = UIEdgeInsets(top: 35, left: 0, bottom: 0, right: 0);
        
        myTableView.delegate = self
        myTableView.dataSource = self
        
        updateLayoutNavAndTable()
        // NOTE: - Registering the cell programmatically
        myTableView.register(TableCell.self, forCellReuseIdentifier: "custom")
    }
}

// MARK: - Table View Delegate
extension MainViewController: UITableViewDelegate {
    
}

// MARK: - Table View Data Source
extension MainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.myTableView.dequeueReusableCell(withIdentifier: "custom") as! TableCell
        cell.configure(with: cellData[indexPath.row])
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // CHECK REUSABLE CELL
        if cellData[indexPath.row].isReached {
            return 190
        } else {
            return 150
        }
    }
}

extension MainViewController {
    func updateLayoutNavAndTable() {
        myTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        myTableView.translatesAutoresizingMaskIntoConstraints = false
        myTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0).isActive = true
        myTableView.bottomAnchor.constraint(equalTo:   view.bottomAnchor,   constant: 0.0).isActive = true
        myTableView.topAnchor.constraint(equalTo:      view.topAnchor,      constant: 0.0).isActive = true
        myTableView.leadingAnchor.constraint(equalTo:  view.leadingAnchor,  constant: 0.0).isActive = true
        (myTableView.contentSize.height > myTableView.frame.size.height) ?
            (myTableView.isScrollEnabled = false) : (myTableView.isScrollEnabled = true)
    }
}
