import UIKit

let API_MAIN_URL: String = "https://intern-f6251.firebaseio.com/intern/metric.json"
let API_SECOND_URL: String = "http://142.93.160.86:8082/api/onetrak_json"
let SEPARATOR_COLOR: UIColor = UIColor(red: 0.70, green: 0.70, blue: 0.70, alpha: 1.0)
let CELL_COLOR: UIColor = UIColor(red: 0.98, green: 0.98, blue: 0.98, alpha: 1.0)
let STEPS_COLOR: UIColor = UIColor(red: 0.64, green: 0.87, blue: 0.94, alpha: 1.0)
let AEROBIC_COLOR: UIColor = UIColor(red: 0.27, green: 0.73, blue: 0.87, alpha: 1.0)
let RUN_COLOR: UIColor = UIColor(red: 0.16, green: 0.44, blue: 0.53, alpha: 1.0)
