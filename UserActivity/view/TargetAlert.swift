import UIKit
class TargetAlert: UIAlertController {
}
extension MainViewController {
    @objc func alertFunction() {
        var targetTextField: UITextField?
        let alertController = UIAlertController(
            title: "Target steps",
            message: "Please enter your taret steps",
            preferredStyle: .alert)
      
        let loginAction = UIAlertAction(
        title: "Set", style: .default) {
            (action) -> Void in
            
            if let target = targetTextField?.text {
                if let integer = Int(target) {
                    Defaults.targetSteps = integer
                    for (index, _) in self.cellData.enumerated() {
                        self.cellData[index].stepsTarget = integer
                    }
                    self.myTableView.reloadData()
                }
                
            } else {
                print("No target entered")
                
            }
        }
        alertController.addTextField {
            (txtUsername) -> Void in
            targetTextField = txtUsername
            targetTextField!.placeholder = "Write your target steps"
            targetTextField?.keyboardType = UIKeyboardType.numberPad
        }
        alertController.addAction(loginAction)
        present(alertController, animated: true, completion: nil)
    }
}
