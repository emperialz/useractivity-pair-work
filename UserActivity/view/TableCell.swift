import UIKit

class TableCell: UITableViewCell {
    
    var cellModel: CellDataModel?
    var dayDate: String?
    var stepsCounter: Int?
    var stepsTarget: Int?
    var walkSteps: Int?
    var aerobicSteps: Int?
    var runSteps: Int?
    var isReached: Bool?
    var barArray: [CGFloat]?
    
    var walkBarWidthConstraint: NSLayoutConstraint?
    var aerobicBarWidthConstraint: NSLayoutConstraint?
    var runBarWidthConstraint: NSLayoutConstraint?
    
    var dayDateView: UILabel = {
        var textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 21)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    var totalStepsCounterView: UILabel = {
        var textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 19)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    var targetStepsCounterView: UILabel = {
        var textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 19)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    let targetStepsLabelView: UILabel = {
        var textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 19)
        textView.text = "steps"
        return textView
    }()
    var walkCounterView: UILabel = {
        var textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 19)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    var aerobicCounterView: UILabel = {
        var textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 19)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    var runCounterView: UILabel = {
        var textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 19)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    var lineBarViewWalk: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.frame = CGRect.zero
        view.backgroundColor = UIColor(red: 0.64, green: 0.87, blue: 0.94, alpha: 1.0)
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        view.layer.cornerRadius = 3
        view.clipsToBounds = true
        return view
    }()
    
    var lineBarViewAerobic: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.frame = CGRect.zero
        view.backgroundColor = UIColor(red: 0.27, green: 0.73, blue: 0.87, alpha: 1.0)
        return view
    }()
   
    var lineBarViewRun: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.frame = CGRect.zero
        view.backgroundColor = UIColor(red: 0.16, green: 0.44, blue: 0.53, alpha: 1.0)
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        view.layer.cornerRadius = 3
        view.clipsToBounds = true
        return view
    }()

    let walkLabelView: UILabel = {
        var textView = UILabel()
        textView.text = "walk"
        textView.textColor = UIColor(red: 0.47, green: 0.47, blue: 0.47, alpha: 1.0)
        textView.font = UIFont.systemFont(ofSize: 14)
        return textView
    }()
    let aerobicLabelView: UILabel = {
        var textView = UILabel()
        textView.text = "aerobic"
        textView.textColor = UIColor(red: 0.47, green: 0.47, blue: 0.47, alpha: 1.0)
        textView.font = UIFont.systemFont(ofSize: 14)
        return textView
    }()
    let runLabelView: UILabel = {
        var textView = UILabel()
        textView.text = "run"
        textView.textColor = UIColor(red: 0.47, green: 0.47, blue: 0.47, alpha: 1.0)
        textView.font = UIFont.systemFont(ofSize: 14)
        return textView
    }()
    
    let bottomGoalBorderView: UIView = {
        let vw = UIView()
        vw.backgroundColor = SEPARATOR_COLOR
        return vw
    }()
    let bottomInnerBorderView: UIView = {
        let vw = UIView()
        vw.backgroundColor = SEPARATOR_COLOR
        return vw
    }()
    let bottomLongInnerBorderView: UIView = {
        let vw = UIView()
        vw.backgroundColor = SEPARATOR_COLOR
        vw.isHidden = true
        return vw
    }()
    let topInnerBorderView: UIView = {
        let vw = UIView()
        vw.backgroundColor = SEPARATOR_COLOR
        return vw
    }()
    let iconStarView: UIImageView = {
        let ivw = UIImageView()
        ivw.translatesAutoresizingMaskIntoConstraints = false
        ivw.image = UIImage(named: "icon_star")
        return ivw
    }()
    let goalLabelView: UILabel = {
        let lvw = UILabel()
        lvw.text = "Goal reached"
        lvw.font = UIFont.systemFont(ofSize: 19)
        lvw.textColor = UIColor(red: 0.17, green: 0.45, blue: 0.53, alpha: 1.0)
        return lvw
    }()
    let goalView: UIView = {
        let vw = UIView()
        vw.frame = CGRect.zero
        vw.backgroundColor = CELL_COLOR
        return vw
    }()
    
    let innerView: UIView = {
        let ivw = UIView()
        ivw.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 120)
        ivw.backgroundColor = CELL_COLOR
        ivw.layer.masksToBounds = true
        return ivw
    }()
    // Actual
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.contentView.addSubview(goalView)
        self.contentView.sendSubviewToBack(goalView)
        self.contentView.addSubview(bottomGoalBorderView)
        self.contentView.addSubview(bottomInnerBorderView)
        self.contentView.addSubview(topInnerBorderView)
        self.contentView.addSubview(iconStarView)
        self.contentView.addSubview(goalLabelView)
        self.contentView.addSubview(bottomLongInnerBorderView)
        
        
        self.contentView.addSubview(innerView)
        self.contentView.sendSubviewToBack(innerView)
        
        self.contentView.addSubview(dayDateView)
        self.contentView.addSubview(walkCounterView)
        self.contentView.addSubview(aerobicCounterView)
        self.contentView.addSubview(runCounterView)
        
        self.contentView.addSubview(walkLabelView)
        self.contentView.addSubview(aerobicLabelView)
        
        self.contentView.addSubview(targetStepsLabelView)
        self.contentView.addSubview(targetStepsCounterView)
        self.contentView.addSubview(totalStepsCounterView)
        
        self.contentView.addSubview(runLabelView)
        
        self.contentView.addSubview(lineBarViewWalk)
        self.contentView.addSubview(lineBarViewAerobic)
        self.contentView.addSubview(lineBarViewRun)
        

        // Constraints        
        // Date
        dayDateView.anchor(top: nil, leading: contentView.leadingAnchor, bottom: targetStepsCounterView.bottomAnchor, trailing: nil)

        // Walk
        walkCounterView.anchor(top: nil, leading: nil, bottom: walkLabelView.topAnchor, trailing: nil)
        walkCounterView.centerXAnchor.constraint(equalTo: walkLabelView.centerXAnchor, constant: 0.0).isActive = true
        walkLabelView.anchor(top: nil, leading: innerView.leadingAnchor, bottom: nil, trailing: nil, padding: 50)
        walkLabelView.bottomAnchor.constraint(equalTo: bottomInnerBorderView.topAnchor, constant: -10).isActive = true
        
        // Aerobic
          // Label
        aerobicLabelView.anchor(top: nil, leading: nil, bottom: walkLabelView.bottomAnchor, trailing: nil)
        aerobicLabelView.centerXAnchor.constraint(equalTo: innerView.centerXAnchor).isActive = true
          // Counter
        aerobicCounterView.anchor(top: nil, leading: nil, bottom: aerobicLabelView.topAnchor, trailing: nil)
        aerobicCounterView.centerXAnchor.constraint(equalTo: innerView.centerXAnchor).isActive = true
        
        // Run
        runLabelView.anchor(top: nil, leading: nil, bottom: walkLabelView.bottomAnchor, trailing: innerView.trailingAnchor, padding: 50)
        runCounterView.anchor(top: nil, leading: nil, bottom: runLabelView.topAnchor, trailing: nil)
        runCounterView.centerXAnchor.constraint(equalTo: runLabelView.centerXAnchor, constant: 0.0).isActive = true

        // Total steps
        targetStepsLabelView.anchor(top: nil, leading: nil, bottom: nil, trailing: self.trailingAnchor)
        targetStepsLabelView.topAnchor.constraint(equalTo: topInnerBorderView.bottomAnchor, constant: 10).isActive = true
        
        targetStepsCounterView.anchor(top: nil, leading: nil, bottom: nil, trailing: targetStepsLabelView.leadingAnchor, padding:  0.0)
        targetStepsCounterView.topAnchor.constraint(equalTo: topInnerBorderView.bottomAnchor, constant: 10).isActive = true
        
        totalStepsCounterView.anchor(top: nil, leading: nil, bottom: nil, trailing: targetStepsCounterView.leadingAnchor, padding: 0.0)
        totalStepsCounterView.topAnchor.constraint(equalTo: topInnerBorderView.bottomAnchor, constant: 10).isActive = true
        
        // Line bar
        lineBarViewWalk.centerYAnchor.constraint(equalTo: innerView.centerYAnchor).isActive = true
        lineBarViewWalk.leadingAnchor.constraint(equalTo: innerView.leadingAnchor, constant: 20).isActive = true
        lineBarViewWalk.trailingAnchor.constraint(equalTo: lineBarViewAerobic.leadingAnchor, constant: -5).isActive = true
        lineBarViewWalk.heightAnchor.constraint(equalToConstant: 5).isActive = true
        
        lineBarViewAerobic.centerYAnchor.constraint(equalTo: innerView.centerYAnchor).isActive = true
        lineBarViewAerobic.trailingAnchor.constraint(equalTo: lineBarViewRun.leadingAnchor, constant: -5).isActive = true
        lineBarViewAerobic.heightAnchor.constraint(equalToConstant: 5).isActive = true
        
        lineBarViewRun.centerYAnchor.constraint(equalTo: innerView.centerYAnchor).isActive = true
        lineBarViewRun.trailingAnchor.constraint(equalTo: innerView.trailingAnchor, constant: -20).isActive = true
        lineBarViewRun.heightAnchor.constraint(equalToConstant: 5).isActive = true
        
        
        goalView.anchor(top: innerView.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor, padding: 0)
        goalView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        bottomGoalBorderView.anchor(top: goalView.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor, padding: 0)
        bottomGoalBorderView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        
        bottomInnerBorderView.anchor(top: innerView.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor)
        bottomInnerBorderView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        bottomLongInnerBorderView.anchor(top: innerView.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor, padding: 0)
        bottomLongInnerBorderView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        topInnerBorderView.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor, padding: 0)
        topInnerBorderView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        iconStarView.widthAnchor.constraint(equalTo: iconStarView.heightAnchor).isActive = true
        iconStarView.topAnchor.constraint(equalTo: goalView.topAnchor, constant: 5).isActive = true
        iconStarView.trailingAnchor.constraint(equalTo: goalView.trailingAnchor, constant: -20).isActive = true
        iconStarView.bottomAnchor.constraint(equalTo: goalView.bottomAnchor, constant: -5).isActive = true
        goalLabelView.anchor(top: goalView.topAnchor, leading: goalView.leadingAnchor, bottom: goalView.bottomAnchor, trailing: nil)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    override func prepareForReuse() {
      super.prepareForReuse()
      if let walkBarWidthConstraint = walkBarWidthConstraint {
        lineBarViewWalk.removeConstraint(walkBarWidthConstraint)
      }
      if let aerobicBarWidthConstraint = aerobicBarWidthConstraint {
        lineBarViewAerobic.removeConstraint(aerobicBarWidthConstraint)
      }
      if let runBarWidthConstraint = runBarWidthConstraint {
        lineBarViewRun.removeConstraint(runBarWidthConstraint)
      }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func calculateBarSize(part: CGFloat) -> CGFloat {
        let size = 270 * part
        if size < 1 {
            return 1
        }
        return size
    }
    func configure(with model: CellDataModel) {
        dayDateView.text = model.dayDate
        walkCounterView.text = "\(model.walkSteps)"
        aerobicCounterView.text = "\(model.aerobicSteps)"
        runCounterView.text = "\(model.runSteps)"
        targetStepsCounterView.text = " / \(model.stepsTarget) "
        totalStepsCounterView.text = "\(model.stepsCounter)"
        if model.isReached == false {
            goalLabelView.isHidden = true
            iconStarView.isHidden = true
            goalView.isHidden = true
            bottomGoalBorderView.isHidden = true
            bottomLongInnerBorderView.isHidden = false
        } else if model.isReached == true {
            goalLabelView.isHidden = false
            iconStarView.isHidden = false
            goalView.isHidden = false
            bottomGoalBorderView.isHidden = false
            bottomLongInnerBorderView.isHidden = true
        }
        aerobicBarWidthConstraint = lineBarViewAerobic.widthAnchor.constraint(equalToConstant: calculateBarSize(part: model.bar[1]))
        aerobicBarWidthConstraint!.isActive = true
        runBarWidthConstraint = lineBarViewRun.widthAnchor.constraint(equalToConstant: calculateBarSize(part: model.bar[2]))
        runBarWidthConstraint!.isActive = true
    }

}

extension UIView {
    func anchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, padding: CGFloat = 20.0) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: 0).isActive = true
        }
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: 0).isActive = true
        }
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -padding).isActive = true
        }
    }
}

